'use strict';

var gulp = require('gulp');
var yargs = require('yargs').argv;
var express = require('express');
var liveReload = require('connect-livereload');
var runSequence = require('run-sequence');

var gulpLiveReload = require('gulp-livereload');
var gulpIf = require('gulp-if');
var inject = require('gulp-inject');

var release = yargs.release ? true : false;

const PORT = 3000;
const LIVE_RELOAD_PORT = 37010;

gulp.task('default', ['build', 'serve', 'watch']);

gulp.task('inject', function(done) {
  gulp.src('src/index.html')
    .pipe(inject(gulp.src(['build/**/*.js', 'build/**/*.css'], {read: false}), {
      addRootSlash: false,
      ignorePath: ['build']
    }))
    .pipe(gulp.dest('build'))
    .on('end', done);
});

gulp.task('build', function(done) {
  runSequence('clean', 'copy', 'inject', done);
});

gulp.task('release', function() {
  release = true;
  runSequence('templates', 'build');
});

gulp.task('update', function(done) {
  runSequence('build', 'reload', done);
});

gulp.task('copy', ['copy:js', 'copy:html', 'copy:css']);

gulp.task('copy:html', function(done) {
  gulp.src(['src/**/*.html', '!src/index.html'])
    .pipe(gulp.dest('build'))
    .on('end', done);
});

gulp.task('templates', function() {
  return gulp.src(['src/**/*.html', '!src/index.html'])

    .pipe(require('gulp-angular-templatecache')({
      module: 'app'
    }))
    .pipe(gulp.dest('build'));
});

gulp.task('copy:js', function(done) {
  gulp.src('src/**/*.js')
    .pipe(require('gulp-babel')({
      presets: ['es2015']
    }))
    .pipe(gulpIf(release, require('gulp-uglify')()))
    .pipe(gulpIf(release, require('gulp-concat')('all.js')))
    .pipe(gulp.dest('build'))
    .on('end', done);
});

gulp.task('clean', function() {
  return require('del')(['build']);
});

gulp.task('copy:css', function(done) {
  gulp.src('src/**/*.styl')
    .pipe(require('gulp-stylus')())
    .pipe(gulpIf(release, require('gulp-concat')('all.css')))
    .pipe(gulp.dest('build'))
    .on('end', done);
});

gulp.task('watch', function() {
  return gulp.watch('src/**', ['update']);
});

gulp.task('reload', function() {
  return gulp.src('build/index.html')
    .pipe(gulpLiveReload(LIVE_RELOAD_PORT));
});

gulp.task('serve', function() {
  var server = express();

  //live-reload on source change
  server.use(liveReload({
    port: LIVE_RELOAD_PORT,
    src: `http://localhost:${LIVE_RELOAD_PORT}/livereload.js?snipver=1`
  }));

  server.use(express.static('./build'));

  //serve entire directory to allow access to bower_components on dev server
  server.use(express.static('./'));

  server.all('/*', function(req, res) {
    res.sendFile('build/index.html', {root: __dirname});
  });

  server.listen(PORT);

  gulpLiveReload.listen(LIVE_RELOAD_PORT);

  console.log(`Server running at localhost:${PORT}`);
});
