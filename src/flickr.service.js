'use strict';

angular.module('flickr', [])

  .service('flickrAPI', ['$http', '$q', function($http, $q) {

    this.getAll = tag => {
      return $http.jsonp('https://api.flickr.com/services/feeds/photos_public.gne', {
        cache: true,
        params: {
          tags: tag,
          tagmode: 'all',
          format: 'json',
          jsoncallback: 'JSON_CALLBACK'
        }
      });
    };

    this.findOne = (tag, author, photo) => {
      console.log(tag, author, photo);

      var deferred = $q.defer();
      $http.jsonp('https://api.flickr.com/services/feeds/photos_public.gne', {
        cache: true,
        params: {
          tags: tag,
          tagmode: 'all',
          format: 'json',
          jsoncallback: 'JSON_CALLBACK'
        }
      }).then(response => {
        var item = _.find(response.data.items, el => {
          return _.contains(el.link, author + '/' + photo);
        });

        deferred.resolve(item);
      });

      return deferred.promise;
    };

  }]);
