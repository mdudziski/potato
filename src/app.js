'use strict';

angular.module('app', [
    'flickr',
    'ngRoute'
  ])

  .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true).hashPrefix('!');

    $routeProvider.when('/tag/:tag/:author/:photo', {
      template: `<div flickr-details item="details"></div>`,
      controller: ['$scope', 'flickrAPI', '$routeParams', function($scope, flickrAPI, $routeParams) {
        flickrAPI.findOne($routeParams.tag, $routeParams.author, $routeParams.photo).then(details => {
          if (!details) {
            alert('@TODO: This should be handled');
          }

          $scope.details = details;
        });
      }]
    }).when('/tag/:tag', {
      template: `<div flickr-feed></div>`
    }).otherwise({
      redirectTo: '/tag/potato'
    });

  }])

  .directive('flickrFeed', ['flickrAPI', '$routeParams', function(flickrAPI, $routeParams) {

    return {
      templateUrl: 'flickr-feed.html',
      scope: {},
      link: scope => {
        scope.tag = $routeParams.tag;

        flickrAPI.getAll($routeParams.tag || 'potato').then(response => {
          console.log(response);
          scope.items = response.data.items;
          scope.items.forEach(el => {
            el._id = el.link.replace('https://www.flickr.com/photos/', '');
            el._tags = el.tags.split(' ');
          });
        });
      }
    };

  }])

  .directive('flickrDetails', ['flickrAPI', '$routeParams', '$sce', function(flickrAPI, $routeParams, $sce) {

    return {
      templateUrl: 'flickr.details.html',
      scope: {
        item: '='
      },
      link: scope => {
        scope.tag = $routeParams.tag;

        scope.$watch('item', () => {
          if (scope.item) {
            scope.item._description = $sce.trustAsHtml(scope.item.description);
            scope.item._tags = scope.item.tags.split(' ');
          }
        });

      }
    };

  }])

  .filter('filterByTags', [function() {

    return (val, tags) => {
      if (!_.isString(tags)) {
        return val;
      }

      var tagsArray = _.filter(_.map(tags.split(','), _.trim), _.identity);

      return val.filter(el => {
        return _.every(tagsArray, tag => _.contains(el._tags, tag));
      });

    };

  }]);

